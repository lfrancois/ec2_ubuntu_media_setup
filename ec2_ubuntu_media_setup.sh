#! /bin/bash -ex

# The stdout and stderr of your user-data script is output in /var/log/syslog 
# and you can review this for any success and failure messages. It will contain 
# both things you echo directly in the script as well as output from programs you run.

export HOME_INSTALL="/mnt"
export HOME=$HOME_INSTALL
LOGS="/root/post-install.$(date -I).$$"
exec > $LOGS 2>&1

install_pkg()
{
    DEBIAN_FRONTEND=noninteractive apt-get -y \
        -o DPkg::Options::=--force-confdef \
        -o DPkg::Options::=--force-confold \
        install $@
}

####################################
# install medibuntu to get the good version of ffmpeg
wget --output-document=/etc/apt/sources.list.d/medibuntu.list http://www.medibuntu.org/sources.list.d/$(lsb_release -cs).list 
apt-get --quiet update 
apt-get --yes --quiet --allow-unauthenticated install medibuntu-keyring 
apt-get --quiet update


####################################
# install basic packages
apt-get --quiet update
####################################
# install basic packages
echo "* [$(date -R)] Install basic packages" 
install_pkg tcsh
install_pkg python
install_pkg ruby
install_pkg ruby-rvm
install_pkg imagemagick
install_pkg build-essential
install_pkg cmake 
install_pkg flex 
install_pkg bison 
install_pkg libboost-all-dev 
install_pkg subversion 
install_pkg git
install_pkg mercurial

####################################
# optional packages
echo "* [$(date -R)] Install optional packages" 


####################################
# ffmpeg build
# http://ubuntuforums.org/showthread.php?t=786095
#
# 
install_pkg ffmpeg
install_pkg build-essential
install_pkg checkinstall 
install_pkg git 
#install_pkg libfaac-dev 
install_pkg libjack-jackd2-dev
install_pkg libmp3lame-dev 
install_pkg libopencore-amrnb-dev 
install_pkg libopencore-amrwb-dev 
install_pkg libsdl1.2-dev 
install_pkg libtheora-dev
install_pkg libva-dev 
install_pkg libvdpau-dev 
install_pkg libvorbis-dev 
install_pkg libx11-dev 
install_pkg libxfixes-dev 
install_pkg texi2html 
install_pkg yasm 
install_pkg zlib1g-dev
cd $HOME_INSTALL
rm -rf ffmpeg
rm -rf x264
git clone git://git.videolan.org/x264
cd x264
./configure --enable-static
make
sudo checkinstall --pkgname=x264 --pkgversion="3:$(./version.sh | \
    awk -F'[" ]' '/POINT/{print $4"+git"$5}')" --backup=no --deldoc=yes \
    --fstrans=no --default
cd $HOME_INSTALL
git clone --depth 1 git://source.ffmpeg.org/ffmpeg
cd ffmpeg
#./configure --enable-gpl --enable-libfaac --enable-libmp3lame --enable-libopencore-amrnb \
./configure --enable-gpl --enable-libmp3lame --enable-libopencore-amrnb \
    --enable-libopencore-amrwb --enable-libtheora --enable-libvorbis --enable-libx264 \
    --enable-nonfree --enable-postproc --enable-version3 --enable-x11grab
make
sudo checkinstall --pkgname=ffmpeg --pkgversion="5:$(date +%Y%m%d%H%M)-git" --backup=no \
  --deldoc=yes --fstrans=no --default
hash x264 ffmpeg ffplay ffprobe

####################################
# specific packages
echo "* [$(date -R)] Install specific packages" 
install_pkg condor
install_pkg openexr
install_pkg exrtools
#install_pkg blender
# lux
install_pkg aqsis

####################################
# openimageio repository setup
add-apt-repository --yes ppa:irie/openimageio
apt-get --quiet update
install_pkg libopenimageio-dev
install_pkg openimageio-tools

####################################
# blender setup
# http://wiki.blender.org/index.php/Dev:2.5/Doc/Building_Blender/Linux/Ubuntu/CMake

install_pkg subversion build-essential gettext \
 libxi-dev libsndfile1-dev \
 libpng12-dev libfftw3-dev \
 libopenexr-dev libopenjpeg-dev \
 libopenal-dev libalut-dev libvorbis-dev \
 libglu1-mesa-dev libsdl1.2-dev libfreetype6-dev \
 libtiff4-dev libavdevice-dev \
 libavformat-dev libavutil-dev libavcodec-dev libjack-dev \
 libswscale-dev libx264-dev libmp3lame-dev python3.2-dev \
 libspnav-dev
cd $HOME_INSTALL
mkdir blender-svn
cd blender-svn
svn co https://svn.blender.org/svnroot/bf-blender/trunk/blender
cd $HOME_INSTALL/blender-svn/blender
make

####################################
# opencolorio setup
# http://ocl.atmind.nl/doku.php?id=development:compilation
install_pkg libjpeg8
install_pkg libjpeg8-dev
install_pkg libpng12-dev
install_pkg libfreetype6-dev
install_pkg python-all-dev
install_pkg python3-all-dev
install_pkg kdelibs5-dev
install_pkg libtiff4-dev libjpeg-dev
install_pkg libopenexr-dev
install_pkg libopenjpeg-dev
install_pkg libopenal-dev
install_pkg libspnav-dev 
install_pkg libxi-dev
cd $HOME_INSTALL
mkdir blender-comp
cd blender-comp
git clone http://git.gitorious.org/blender-compositor/blender.git
mkdir cmake
cd cmake
cmake ../blender
make -j 4
cd $HOME_INSTALL
echo "DEV: export OCIO=$HOME_INSTALL/blender-comp/ocio-configs-0.7/spi-vfx/config.ocio"


####################################
# lux setup
# http://www.luxrender.net/release/luxrender/0.8/linux/64/lux-v08-x86_64-sse2-NoOpenCL.tar.bz2
install_pkg libsm6
install_pkg libglu1-mesa
cd $HOME_INSTALL
wget http://www.luxrender.net/release/luxrender/0.8/linux/64/lux-v08-x86_64-sse2-NoOpenCL.tar.bz2
tar xvjf lux-v08-x86_64-sse2-NoOpenCL.tar.bz2
mv lux-v08-x86_64-sse2-NoOpenCL /usr/local/lux

####################################
# mount nfs file systems
echo "* [$(date -R)] mount nfs file systems"

####################################
# load propietary packages from S3 and install them
echo "* [$(date -R)] load propietary packages from S3 and install them"

####################################
# Nuke install
#http://www.studiopyxis.com/downloads/Nuke6.3v6-linux-x86-release-64.tgz
install_pkg libxinerama1
install_pkg libxft2
install_pkg libxi6
install_pkg unzip
cd $HOME_INSTALL
wget http://www.studiopyxis.com/downloads/Nuke6.3v6-linux-x86-release-64.tgz
tar xvzf Nuke6.3v6-linux-x86-release-64.tgz
unzip Nuke6.3v6-linux-x86-release-64-installer -d /usr/local/Nuke6.3v6/ 

#http://www.studiopyxis.com/downloads/Maxwell_2.6.10.0.tar.gz
install_pkg libsm6
install_pkg libssl0.9.8
install_pkg libgl1-mesa-glx
install_pkg libglu1-mesa
cd $HOME_INSTALL
wget http://www.studiopyxis.com/downloads/Maxwell_2.6.10.0.tar.gz
tar xvzf Maxwell_2.6.10.0.tar.gz
./Maxwell_2.6.10.0 --mode silent --prefix /usr/local/maxwell
#MAXWELL2_ROOT=/usr/local/maxwell;export MAXWELL2_ROOT; 
#MAXWELL2_MATERIALS_DATABASE=/usr/local/maxwell/materials\ database;export MAXWELL2_MATERIALS_DATABASE; 

####################################
# Maya install
#http://www.studiopyxis.com/downloads/Autodesk_Maya_2012_English_Linux_64bit.tgz
#http://stefanobolli.blogspot.com/2011/05/maya-2012-x64-on-ubuntu-1104.html
cd $HOME_INSTALL
mkdir maya
cd maya
wget http://www.studiopyxis.com/downloads/Autodesk_Maya_2012_English_Linux_64bit.tgz
install_pkg libxrandr2
install_pkg csh tcsh libglw1-mesa libglw1-mesa-dev mesa-utils libaudiofile-dev libaudiofile0 libaudiofile0-dbg elfutils gamin libxp6 libxpm4 libxt6 libxp6 libxmu6 libxau6 libxinerama1 xfs xfstt ttf-liberation xfonts-100dpi xfonts-75dpi alien
tar xvzf Autodesk_Maya_2012_English_Linux_64bit.tgz
for i in *x86_64.rpm; do alien -cv $i; done
dpkg -i adlmapps*.deb
dpkg -i adlmflexnetserver*.deb
dpkg -i adlmflexnetclient*.deb
dpkg -i maya2012*.deb
dpkg -i composite*.deb
dpkg -i matchmover*.deb
mkdir /usr/tmp
chmod 777 /usr/tmp
sh -c "echo 'setenv LC_ALL en_US.UTF-8' >> /usr/autodesk/maya2012-x64/bin/maya2012"


apt-get --yes --quiet upgrade
echo "* [$(date -R)] The server is up and running!" 
